class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.string :title
      t.text :description
      t.decimal :price_base
      t.integer :stock
      t.string :picture_1
      t.string :picture_2
      t.string :picture_3
      t.boolean :available

      t.timestamps
    end
  end
end
