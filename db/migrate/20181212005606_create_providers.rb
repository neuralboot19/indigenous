class CreateProviders < ActiveRecord::Migration[5.1]
  def change
    create_table :providers do |t|
      t.string :first_name
      t.string :last_name
      t.decimal :identification
      t.string :email

      t.timestamps
    end
  end
end
