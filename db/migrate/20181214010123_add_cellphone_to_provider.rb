class AddCellphoneToProvider < ActiveRecord::Migration[5.1]
  def change
    add_column :providers, :cellphone, :string
  end
end
