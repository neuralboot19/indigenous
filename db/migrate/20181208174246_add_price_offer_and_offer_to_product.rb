class AddPriceOfferAndOfferToProduct < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :price_offer, :decimal
    add_column :products, :offer, :boolean, default: false
  end
end
