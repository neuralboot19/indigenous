class AddCityToProduct < ActiveRecord::Migration[5.1]
  def change
    add_reference :products, :city, foreign_key: true
  end
end
