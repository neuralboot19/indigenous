module ProductsHelper
  include Pagy::Frontend
  require 'pagy/extras/bootstrap'
  
  def set_images(picture1, picture2, picture3)
		images = Array.new
		images.push(picture1) unless picture1.nil? || picture1.blank?
		images.push(picture2) unless picture2.nil? || picture2.blank?
		images.push(picture3) unless picture3.nil? || picture3.blank?
		return images
  end
  
end
