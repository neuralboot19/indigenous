class ProductsController < ApplicationController
  include Pagy::Backend
  before_action :set_product, only: %i[show]
  before_action :all_categories
  
  def index
    @pagy, @products = pagy(Product.all, page: params[:page], items: 12)
  end
  
  private

  def set_product
    @product = Product.find(params[:id])
  end

  def all_categories
    @categories = Category.all()
  end
end
