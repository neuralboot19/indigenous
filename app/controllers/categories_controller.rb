class CategoriesController < ApplicationController
  include Pagy::Backend
  before_action :set_category, only: %i[show]
  before_action :all_categories

  def index
    @pagy, @products = pagy(Product.all, page: params[:page], items: 8)
  end

  def show
    @pagy, @products = pagy(Product.where(category_id: params[:id]), page: params[:page], items: 12)
  end

  private

  def set_category
    @category = Category.find(params[:id])
  end

  def all_categories
    @categories = Category.all()
  end
end
