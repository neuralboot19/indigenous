class HomeController < ApplicationController
  include Pagy::Backend
  protect_from_forgery with: :exception

  before_action :set_product, only: %i[show]
  before_action :all_categories

  def index
    @filter = params[:filter]
    @pagy, @products = @filter.nil? ? pagy(Product.where(available: true), page: params[:page], items: 4) : pagy(Product.where(available: true).filter_by_style(@filter), page: params[:page], items: 4)
  end
  
  private
  
  def set_product
    @product = Product.find(params[:id])
  end

  def all_categories
    @categories = Category.all()
  end
end
