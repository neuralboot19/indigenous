class OffersController < ApplicationController
  include Pagy::Backend
  protect_from_forgery with: :exception
  before_action :all_categories

  def index
    @filter = params[:filter]
    @pagy, @products = @filter.nil? ? pagy(Product.where(available: true, offer: true), page: params[:page], items: 12) : pagy(Product.where(available: true, offer: true).filter_by_style(@filter), page: params[:page], items: 12)
  end

  private

  def all_categories
    @categories = Category.all()
  end
end