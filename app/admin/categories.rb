ActiveAdmin.register Category do
  permit_params :name, :title, :description, :image

  index do
    selectable_column
    id_column
    column :name
    column :title
    column :description
    column :image do |c|
      image_tag(c.image.url(:thumbnail))
    end
    actions
  end

  filter :name
  filter :title

  form do |f|
    f.inputs do
      f.input :name
      f.input :title
      f.input :description
      f.hidden_field :image_cache
      f.file_field :image, style: "margin-bottom: 20px; margin-left: 25%;"
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :title
      row :description
      row :image do |c|
        image_tag(c.image.url(:thumbnail))
      end
    end
  end
end
