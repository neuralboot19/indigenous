ActiveAdmin.register Product do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :name, :title, :description, :price_base, :stock, :picture_1, :picture_2, :picture_3, :available, :category_id, :price_offer, :offer, :provider_id, :city_id

  index do
    selectable_column
    id_column
    column :name
    column :provider do |p|
      p.provider ? "#{p.provider.first_name} #{p.provider.last_name}" : ""
    end
    column :city
    column :price_base
    column :stock
    column :picture_1 do |p|
      image_tag(p.picture_1.url(:standard))
    end
    column :available
    column :offer
    actions
  end

  filter :price_base
  filter :category
  filter :provider
  filter :city

  form do |f|
    f.inputs do
      f.input :name
      f.input :title
      f.input :description
      f.input :price_base
      f.input :price_offer
      f.input :stock
      f.input :category
      f.input :provider_id, :label => 'Provider', :as => :select, :collection => Provider.all.map{|p| ["#{p.last_name}, #{p.first_name}", p.id]}
      f.input :city
      f.inputs do
        f.hidden_field :picture_1_cache
        f.file_field :picture_1, style: "margin-bottom: 20px; margin-left: 25%;"
        f.hidden_field :picture_2_cache
        f.file_field :picture_2, style: "margin-bottom: 20px; margin-left: 17%;"
        f.hidden_field :picture_3_cache
        f.file_field :picture_3, style: "margin-bottom: 20px; margin-left: 17%;"
      end
      f.input :available
      f.input :offer
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :title
      row :description
      row :price_base
      row :stock
      row :category
      row :provider do |p|
        "#{p.provider.first_name} #{p.provider.last_name}"
      end
      row :city
      row :picture_1 do |p|
        image_tag(p.picture_1.url(:standard))
      end
      row :picture_2 do |p|
        image_tag(p.picture_2.url(:standard))
      end
      row :picture_3 do |p|
        image_tag(p.picture_3.url(:standard))
      end
      row :available
      row :offer
    end
  end

end
