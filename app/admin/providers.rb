ActiveAdmin.register Provider do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
  permit_params :first_name, :last_name, :identification, :email, :cellphone

  index do
    selectable_column
    id_column
    column :first_name
    column :last_name
    column :identification
    column :email
    column :cellphone
    actions
  end

  filter :email

  form do |f|
    f.inputs do
      f.input :first_name
      f.input :last_name
      f.input :identification
      f.input :email
      f.input :cellphone
    end
    f.actions
  end

  show do
    attributes_table do
      row :first_name
      row :last_name
      row :identification
      row :email
      row :cellphone
    end
  end

end
