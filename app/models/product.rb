class Product < ApplicationRecord
    mount_uploader :picture_1, ImageUploader
    mount_uploader :picture_2, ImageUploader
    mount_uploader :picture_3, ImageUploader
    belongs_to :category
    belongs_to :provider
    belongs_to :city
end
