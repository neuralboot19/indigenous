class Category < ApplicationRecord
  mount_uploader :image, ImageUploader
  has_many :products

  accepts_nested_attributes_for :products, allow_destroy: true

  # scopes
  scope :filter_by_style, -> (tag) { joins(:tags).where(tags: {name: tag})}
  default_scope { eager_load(:products) }
end
