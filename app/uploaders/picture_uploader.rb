class PictureUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave

  process :convert => 'png'
  process :tags => ['picture_product']

  version :thumbnail do
    process :resize_to_fill => [200, 200]
  end

  version :standard do
    resize_to_fit(50, 50)
  end

  def store_dir
    return model.name.sub!(" ", "_")
  end
end
